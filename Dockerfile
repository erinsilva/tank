FROM node:fermium-alpine
WORKDIR /app
COPY . .

RUN npm ci
CMD ["npm", "run", "start"]
