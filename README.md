# Tank Game

Based on this [pixijs boilerplate](https://github.com/yordan-kanchelov/pixi-typescript-boilerplate) 

## How to Play

- Press "T" to switch tank between red, blue and green.
- Press "R" to restart the game.
- Each tank has different amount of bullets
  - red = 2
  - blue = 3
  - green = 1
- Each tank causes different damage per bullet
  - red = 10
  - blue = 20
  - green = 25
- You cannot go through obstacles.
- You can destroy a hay, which has 100 health.
- There will be (randomly positioned through the board) 25 hays and 50 walls.
- The board is 50x50 and each block is 35x35px.

## Commands

- `npm run build` - starts build procedure
- `npm run start` - start watching for files and open's server on localhost:8080

## Docker
You can use Docker to easily build and run this application, using the following commands: 
```
docker build -t tank-game  .
docker run -p 8080:8080 -d tank-game
```

Then you can reach the application [here](http://localhost:8080).
