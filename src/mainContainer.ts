import * as PIXI from "pixi.js";
import Board from "./components/board";
import GameUtils from "./utils/gameUtils";
import GameConfig from "./config/gameConfig";

class MainContainer extends PIXI.Container {
    private popUpLayer: PIXI.Container | undefined;
    private stateLayer: PIXI.Container | undefined;
    private uiLayer: PIXI.Container | undefined;

    constructor() {
        super();
        this.addLayers();
        this.fillStateLayer();
        this.fillUiLayer();
    }

    private addLayers() {
        this.stateLayer = new PIXI.Container();
        this.popUpLayer = new PIXI.Container();
        this.uiLayer = new PIXI.Container();
        [this.stateLayer, this.popUpLayer, this.uiLayer].forEach((l) => this.addChild(l));
    }

    private fillStateLayer() {
        // betsScreen + drawScreen
        const board = new Board();
        board.position.set(board.width/2+50, board.height/2+50);
        board.on("restartGame", ()=> this.restartGame());
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        this.stateLayer.addChild(board);
    }

    private restartGame(){
        console.log("restarting")
        if(this.stateLayer && this.stateLayer.children[this.stateLayer.children.length-1]) this.stateLayer.removeChildren();
        this.fillStateLayer();
    }

    private fillUiLayer() {
        // ui permanent such as settings buttons
        GameUtils.showText({
            text: "R = Restart",
            color: GameUtils.color("red"),
            size: 40,
            x: GameConfig.gameWidth-20,
            y: 20,
            align: "right",
            parent: this.uiLayer,
        });

        GameUtils.showText({
            text: "T = Switch Tank",
            color: GameUtils.color("red"),
            size: 40,
            x: GameConfig.gameWidth-20,
            y: 60,
            align: "right",
            parent: this.uiLayer,
        });

        GameUtils.showText({
            text: "F = Fire",
            color: GameUtils.color("red"),
            size: 40,
            x: GameConfig.gameWidth-20,
            y: 100,
            align: "right",
            parent: this.uiLayer,
        });
    }
}
export default MainContainer;
