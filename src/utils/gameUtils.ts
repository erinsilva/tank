﻿import * as PIXI from "pixi.js";
import ColorConfig from "../config/colorConfig";
import * as EventEmitter from "eventemitter3";

const GameUtils = {
    eventEmitter: new EventEmitter.EventEmitter(),
    
    color(colorName:string) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return ColorConfig[colorName];
    },
    showText(params:any) {
        const {
            text, font, size, style, color, align, valign, x = 0, y = 0,
            parent, wordWrap, textWidth, weight, alpha = 1, lineHeight,
            letterSpacing
        } = params;

        const txt = new PIXI.Text(text + "", {
            fontStyle: style || "normal",
            fontFamily: font || "Arial",
            fontSize: size || 24,
            fill: color || 0x000000,
            wordWrap: wordWrap || textWidth || (text + "").indexOf("\\n") !== -1,
            wordWrapWidth: textWidth || 10000,
            fontWeight: weight || (size >= 20) ? "600" : "100",
            align: align || 'center',
            lineHeight: lineHeight || null,
            letterSpacing: letterSpacing || 0
        });

        txt.anchor.set(
            align == "left" ? 0 : (align == "right" ? 1 : 0.5),
            valign == "top" ? 0 : (valign == "bottom" ? 1 : 0.5)
        );

        txt.alpha = alpha;
        txt.position.set(x, y);

        return parent ? parent.addChild(txt) : txt;
    },

};

export default GameUtils;
