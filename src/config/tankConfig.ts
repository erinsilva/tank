const tankConfig = [
    {
        color: "red",
        damage: 10,
        bullets: 2,
    },
    {
        color: "blue",
        damage: 20,
        bullets: 3
    },
    {
        color: "green",
        damage: 25,
        bullets: 1
    }
];

export default tankConfig;