enum GameConfig {
    gameWidth = 2200,
    gameHeight = 1800,
    boardWidth = 50,
    boardHeight = 50,
    blockSize = 35,
    numHays = 25,
    numWalls = 50,
}

export default GameConfig;