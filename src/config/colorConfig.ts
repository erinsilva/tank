enum ColorConfig {
    white = 0xFFFFFF,
    red = 0xFF2457,
    blue = 0x009CFF,
    green = 0x478863,
    darkGreen = 0x0E7239,
    grey = 0xE6E6E6,
    beige = 0xE5C337,
}

export default ColorConfig;