import * as PIXI from "pixi.js";
import "./style.css";
import MainContainer from "./mainContainer";
import GameConfig from "./config/gameConfig";

const gameWidth = GameConfig.gameWidth;
const gameHeight = GameConfig.gameHeight;

const app = new PIXI.Application({
    backgroundColor: 0xd3d3d3,
    width: gameWidth,
    height: gameHeight,
});

const stage = app.stage;

window.onload = async (): Promise<void> => {
    document.body.appendChild(app.view);
    resizeCanvas();
    const mainContainer = new MainContainer();
    stage.addChild(mainContainer);
};
app.ticker.add(function(delta) {
    // Rotate mr rabbit clockwise
    //bunny.rotation += 0.1 * delta;
});

function resizeCanvas(): void {
    const resize = () => {
        const renderer = app.renderer;
        const rendererSize = [GameConfig.gameWidth, GameConfig.gameHeight];

        let ratio = rendererSize[0] / rendererSize[1];
        let w, h;

        if (window.innerWidth / window.innerHeight >= ratio) {
            w = window.innerHeight * ratio;
            h = window.innerHeight;
        } else {
            w = window.innerWidth;
            h = window.innerWidth / ratio;
        }

        if (renderer) {
            renderer.view.style.width = w + 'px';
            renderer.view.style.height = h + 'px';
        }
    };

    resize();

    window.addEventListener("resize", resize);
}
