import * as PIXI from "pixi.js";
import {Position} from "./types";
import GameConfig from "../config/gameConfig";


export default class Obstacle extends PIXI.Graphics {

    protected obstacleLayer: PIXI.Container = new PIXI.Container();
    protected obstacle: PIXI.Graphics = new PIXI.Graphics();
    private obstaclePosition: Position = {x: 0, y: 0};

    constructor() {
        super();
        this.addChild(this.obstacleLayer);
    }

    protected draw(obstacleGraphics : PIXI.Graphics){
        this.obstacle = obstacleGraphics;
    }

    protected init() {
        this.obstacleLayer.addChild(this.obstacle);
        this.obstacle.pivot.set(this.obstacle.width/2, this.obstacle.width/2);
        this.obstacle.position.set(0,0)
    }

    public setPosition(position: Position) {
        this.obstaclePosition = position;
        this.position.set(position.x * GameConfig.blockSize, position.y * GameConfig.blockSize);
    }

    public getPosition(): Position {
        return this.obstaclePosition;
    }

}
