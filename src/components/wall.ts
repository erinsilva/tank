import * as PIXI from "pixi.js";
import Obstacle from "./obstacle";

export default class Wall extends Obstacle {

    constructor() {
        super();

        this.drawWall();
        this.init();
        this.obstacleLayer.addChild(this);

    }

    private drawWall(){
        this.draw(new PIXI.Graphics()
            .beginFill(0xD64729)
            .drawRect(0, 0, 35, 35));
    }
}