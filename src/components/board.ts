import * as PIXI from "pixi.js";
import Tank from "./tank";
import BoardBlock from "../components/boardBlock";
import tankConfig from "../config/tankConfig";
import Obstacle from "./obstacle";
import Hay from "./hay";
import Wall from "./wall";
import {Direction, Position} from "./types";
import GameConfig from "../config/gameConfig";
import Bullet from "./bullet";

export default class Board extends PIXI.Container {

    private blocksLayer: PIXI.Container = new PIXI.Container();
    private tankLayer: PIXI.Container = new PIXI.Container();
    private bulletLayer: PIXI.Container = new PIXI.Container();
    private obstacleLayer: PIXI.Container = new PIXI.Container();

    private obstaclesArray : Array <Obstacle> = [];
    private shouldFire: boolean = false;
    private lastShot: number = 0;

    // @ts-ignore
    private tank : Tank;

    constructor() {
        super();
        this.addLayers();
        this.addBlocks();
        this.addTank(0);
        this.addObstacle();
        this.positionBoard();

        window.addEventListener("keyup", this.onKeyUp.bind(this), false);
        window.addEventListener("keydown", this.onKeyDown.bind(this), false);
    }

    private addLayers(){
        [this.blocksLayer, this.tankLayer, this.obstacleLayer, this.bulletLayer].forEach((l) => this.addChild(l));
    }

    private addBlocks(){

        for(let xPos=0; xPos<GameConfig.boardWidth; xPos++) {
            for(let yPos=0; yPos<GameConfig.boardHeight; yPos++) {
                const block = new BoardBlock({ number:xPos*yPos });
                this.blocksLayer.addChild(block);
                block.position.set(GameConfig.blockSize * xPos, GameConfig.blockSize * yPos);
            }
        }
    }

    private addTank(tankNumber:number=0){
        // adding red as default
        const tankInitialPosition: Position = {
            x: Math.floor(GameConfig.boardWidth/2),
            y: Math.floor(GameConfig.boardHeight/2)
        };
        const tankInitialDirection: Direction = {
            x: 0,
            y: -1
        };
        this.tank = new Tank(
            { tankNumber },
            tankInitialPosition, tankInitialDirection);
        this.tankLayer.addChild(this.tank);
    }

    private addObstacle(){

        for(let i=0; i<GameConfig.numWalls; i++){
            const wall = new Wall()
            this.obstacleLayer.addChild(wall);
            this.obstaclesArray.push(wall);

            let newPos: Position;
            do {
                newPos = {
                    x: Math.floor(Math.random() * GameConfig.boardWidth),
                    y: Math.floor(Math.random() * GameConfig.boardHeight)
                };
            } while (this.detectObstacleCollision(newPos))

            wall.setPosition(newPos);
        }

        for(let i=0; i<GameConfig.numHays; i++){
            const hay = new Hay();
            this.obstacleLayer.addChild(hay);
            this.obstaclesArray.push(hay);

            let newPos: Position;
            do {
                newPos = {
                    x: Math.floor(Math.random() * GameConfig.boardWidth),
                    y: Math.floor(Math.random() * GameConfig.boardHeight)
                };
            } while (this.detectObstacleCollision(newPos))
            hay.setPosition(newPos);
        }

    }

    private positionBoard(){
        const layersPosition = { x: -this.blocksLayer.width/2, y: -this.blocksLayer.height/2 };
        [this.blocksLayer, this.tankLayer, this.obstacleLayer, this.bulletLayer].forEach((l) => l.position.set(layersPosition.x, layersPosition.y));

    }

    private onKeyUp(keyboardCode: any) {
        if(keyboardCode.key==="f"){
            this.shouldFire = false;
        }
    }

    private onKeyDown(keyboardCode: any) {

        if(keyboardCode.key==="t"){
            this.changeTankState();
        }

        if(keyboardCode.key==="r"){
            this.emit("restartGame");
        }

        const tankPosition = this.tank.getPosition();

        if(keyboardCode.key==="f"){
            console.log("FIRE", this.bulletLayer.children.length);
            this.shouldFire = true;
        }

        if(keyboardCode.key==="ArrowLeft"){
            this.tank.setDirection({
                x: -1,
                y: 0
            });
            const newTankPosition = {
                x: tankPosition.x - 1,
                y: tankPosition.y
            }
            if(tankPosition.x > 0 && !this.detectObstacleCollision(newTankPosition)) {
                this.tank.setPosition(newTankPosition);
            }
        }
        if(keyboardCode.key==="ArrowRight"){
            this.tank.setDirection({
                x: 1,
                y: 0
            });
            const newTankPosition = {
                x: tankPosition.x + 1,
                y: tankPosition.y
            }
            if(tankPosition.x < GameConfig.boardWidth - 1 && !this.detectObstacleCollision(newTankPosition)) {
                this.tank.setPosition(newTankPosition);
            }
        }
        if(keyboardCode.key==="ArrowUp"){
            this.tank.setDirection({
                x: 0,
                y: -1
            });
            const newTankPosition = {
                x: tankPosition.x,
                y: tankPosition.y - 1
            }
            if(tankPosition.y > 0 && !this.detectObstacleCollision(newTankPosition)) {
                this.tank.setPosition(newTankPosition);
            }
        }
        if(keyboardCode.key==="ArrowDown"){
            this.tank.setDirection({
                x: 0,
                y: 1
            });
            const newTankPosition = {
                x: tankPosition.x,
                y: tankPosition.y + 1
            }
            if(tankPosition.y < GameConfig.boardHeight - 1 && !this.detectObstacleCollision(newTankPosition)) {
                this.tank.setPosition(newTankPosition);
            }
        }
    }

    updateTransform(): void {

        this.bulletLayer.children.forEach((bullet)=> {
            if(bullet.visible === false) {
                this.bulletLayer.removeChild(bullet);
                bullet.destroy(true);
            }
        });

        if(this.shouldFire) {
            const now = Date.now();
            if (now - this.lastShot > 100 && this.bulletLayer.children.length < this.tank.getNumberBullets()) {
                let bullet = new Bullet(this.tank.getPosition(), this.tank.getDirection(), this);
                this.bulletLayer.addChild(bullet);
                this.lastShot = now;
            }
        }

        super.updateTransform();
    }

    public handleBulletCollision(bulletPosition: Position){
        const obstacleHit = this.obstaclesArray.find((obstacle)=> obstacle.getPosition().x === bulletPosition.x && obstacle.getPosition().y === bulletPosition.y);
        if(obstacleHit instanceof Hay){
            obstacleHit.setHealth(obstacleHit.getHealth()-this.tank.getDamageValue());
            if(obstacleHit.getHealth() <=0){
                this.obstacleLayer.removeChild(obstacleHit);
                this.obstaclesArray.splice(this.obstaclesArray.indexOf(obstacleHit), 1);
                obstacleHit.destroy(true);
            }
        }

    }

    public detectObstacleCollision(position: Position): boolean {
        for (let obj of this.obstaclesArray) {
            if(obj.getPosition().x === position.x && obj.getPosition().y === position.y){
                return true;
            }
        }
        return false;
    }

    private changeTankState(){
        let newTankNumber = this.tank.getTankNumber() + 1;
        if (newTankNumber >= tankConfig.length) {
            newTankNumber = 0;
        }
        this.tankLayer.removeChildren();
        this.addTank(newTankNumber);
    }
}