import * as PIXI from "pixi.js";
import GameUtils from "../utils/gameUtils";
import Obstacle from "./obstacle";

export default class Hay extends Obstacle {

    private health : number = 100;

    constructor() {
        super();

        this.drawHay();
        this.init();
        this.obstacleLayer.addChild(this);
    }

    public drawHay(){
        this.draw(new PIXI.Graphics()
            .lineStyle(1, GameUtils.color("beige"))
            .moveTo(4, 5)
            .lineTo(4, 34)
            .moveTo(10, 5)
            .lineTo(10, 34)
            .moveTo(16, 5)
            .lineTo(16, 34)
            .moveTo(22, 5)
            .lineTo(22, 34)
            .moveTo(28, 5)
            .lineTo(28, 34)
            .moveTo(32, 5)
            .lineTo(32, 34)
            .moveTo(2, 5)
            .lineTo(34, 5)
            .moveTo(2, 15)
            .lineTo(34, 15)
            .moveTo(2, 25)
            .lineTo(34, 25)
            .moveTo(2, 34)
            .lineTo(34, 34));
    }

    public getHealth(){
        return this.health;
    }

    public setHealth(healthValue:number){
        this.health = healthValue;
    }
}