import * as PIXI from "pixi.js";
import GameUtils from "../utils/gameUtils";
import gameConfig from "../config/gameConfig";

interface NumberButtonParams {
    number : number;
}

export default class BoardBlock extends PIXI.Graphics {
    private blockNumber : number;
    // @ts-ignore
    constructor(params: NumberButtonParams = { number }) {
        super();
        this.blockNumber = params.number;
        this.init();

    }

    private init() {
        const boardBlockSize = gameConfig.blockSize;

        const block = new PIXI.Graphics()
            .beginFill(GameUtils.color("green"), 1)
            .lineStyle(1, GameUtils.color("darkGreen"))
            .drawRect(0, 0, boardBlockSize, boardBlockSize);

        this.addChild(block);
        block.pivot.set(block.width/2, block.width/2);
    }

}
