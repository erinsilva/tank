import * as PIXI from "pixi.js";
import GameUtils from "../utils/gameUtils";
import GameConfig from "../config/gameConfig";
import {Direction, Position} from "./types";
import Board from "./board";

export default class Bullet extends PIXI.Sprite {
    private lastUpdate;
    private boardRef : Board | undefined;
    private bulletPosition : Position = { x: 0, y: 0 };
    private bulletDirection : Direction = { x: 0, y: 0 };

    private bullet: PIXI.Graphics =new PIXI.Graphics()
        .lineStyle(1, GameUtils.color("white"))
        .beginFill(0xe74c3c)
        .drawCircle(0, 0, GameConfig.blockSize/4)
        .endFill();

    constructor(bulletPosition: Position, direction: Direction, boardClass : Board) {
        super();
        this.bulletPosition = bulletPosition;
        this.bulletDirection = direction;
        this.boardRef = boardClass;

        // @ts-ignore
        this.drawBullet();
        this.lastUpdate = Date.now();
    }

    private drawBullet(){
        this.addChild(this.bullet);
        this.bullet.pivot.set(GameConfig.blockSize/2, GameConfig.blockSize/2);
        this.bullet.position.set(
            this.bulletPosition.x * GameConfig.blockSize + GameConfig.blockSize/2,
            this.bulletPosition.y * GameConfig.blockSize + GameConfig.blockSize/2);
    }

    updateTransform(): void {
        if(!this.visible) return;

        let now = Date.now();
        const elapsed = (now - this.lastUpdate)/1000.0;
        this.lastUpdate = Date.now();


        if(this.bulletDirection.x === 1) this.bullet.x += GameConfig.blockSize * 5 * elapsed;
        if(this.bulletDirection.y === 1) this.bullet.y += GameConfig.blockSize * 5 * elapsed;
        if(this.bulletDirection.x === -1) this.bullet.x -= GameConfig.blockSize * 5 * elapsed;
        if(this.bulletDirection.y === -1) this.bullet.y -= GameConfig.blockSize * 5 * elapsed;

        this.bulletPosition = {
            x: Math.floor(this.bullet.x / GameConfig.blockSize),
            y: Math.floor(this.bullet.y / GameConfig.blockSize)
        }
        if(this.isOutOfBoundaries()) {
            this.visible = false;
        }
        if(this.boardRef?.detectObstacleCollision(this.bulletPosition)){
            this.visible = false;
            this.boardRef?.handleBulletCollision(this.bulletPosition);
        }
        super.updateTransform();

    }

    private isOutOfBoundaries() {

        if (this.bulletPosition.x > GameConfig.boardWidth ||
            this.bulletPosition.x < 0 ||
            this.bulletPosition.y < 0 ||
            this.bulletPosition.y > GameConfig.boardHeight){
            return true;
        }else{
            return false;
        }
    }
}