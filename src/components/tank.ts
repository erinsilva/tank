import * as PIXI from "pixi.js";
import GameUtils from "../utils/gameUtils";
import {Direction, Position} from "./types";
import GameConfig from "../config/gameConfig";
import TankConfig from "../config/tankConfig";

interface TankObj {
    tankNumber: number
}

export default class Tank extends PIXI.Graphics {

    private tankLayer: PIXI.Container = new PIXI.Container();
    private tankPosition: Position = {x: 0, y: 0};
    private tankDirection: Direction = {x: 0, y: -1};

    private tankNumber : number = 0;

    constructor(params: TankObj = { tankNumber: 0 }, initialPosition: Position, initialDirection: Direction) {
        super();
        this.tankNumber = params.tankNumber;
        this.init(params);
        this.setPosition(initialPosition);
        this.setDirection(initialDirection);
        this.addChild(this.tankLayer);
    }

    public setPosition(position: Position) {
        this.tankPosition = position;
        this.position.x = GameConfig.blockSize * position.x;
        this.position.y = GameConfig.blockSize * position.y;
    }

    public getPosition(): Position {
        return this.tankPosition;
    }

    public getTankNumber(): number {
        return this.tankNumber;
    }

    public setDirection(direction: Direction) {
        this.tankDirection = direction;
        if (direction.x == -1) {
            this.rotation = -Math.PI/2;
        } else if (direction.x == 1) {
            this.rotation = Math.PI/2;
        } else if (direction.y == 1) {
            this.rotation = Math.PI;
        } else {
            this.rotation = 0;
        }
    }

    public getDirection(): Direction {
        return this.tankDirection;
    }

    public getNumberBullets(): number {
        return TankConfig[this.tankNumber].bullets;
    }

    public getDamageValue(): number {
        return TankConfig[this.tankNumber].damage;
    }

    private init(params: TankObj) {

        let tank = new PIXI.Graphics()
            .beginFill(GameUtils.color(TankConfig[params.tankNumber].color))
            .lineStyle(1, GameUtils.color("black"))
            .drawRoundedRect(0, 0, 35, 35, 5)
            .lineStyle(4, GameUtils.color("black"))
            .endFill()
            .moveTo(18, 0)
            .lineTo(18, 18)
            .lineStyle(2, GameUtils.color("black"))
            .drawRect(10, 5, 15, 20)
            .moveTo(5, 5)
            .lineTo(5, 34)
            .moveTo(32, 5)
            .lineTo(32, 34);
        this.addChild(tank);

        tank.pivot.set(tank.width/2, tank.width/2);
        tank.position.set(0,0)
    }
}
